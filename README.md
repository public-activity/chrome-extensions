# Chrome extensions

Various simple chrome extensions:

- LenoblPasteEnable - provide the ability to insert text on site https://lenobl.ru/
- LettersGovSpbPasteEnable - provide the ability to insert text on site https://letters.gov.spb.ru/
